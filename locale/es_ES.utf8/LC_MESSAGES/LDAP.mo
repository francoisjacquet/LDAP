��    
      l      �       �      �                .     G     `  	   p  Y   z     �  -  �          +     @     U     n     �  	   �  \   �                           	                   
    Bind DN (Username) Is Active Directory? LDAP authentication. LDAP authentication: %s. LDAP connection: Syntax. LDAP server URI LDAP: %s. PHP extensions: LDAP plugin relies on the ldap extension. Please install and activate it. User base DN Project-Id-Version: LDAP plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:14+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-Bookmarks: -1,-1,3,-1,-1,-1,-1,-1,-1,-1
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Bind DN (Nombre de Usuario) Es Active Directory? Autenticación LDAP. Autenticación LDAP: %s. Conexión LDAP: sintaxis. URI del servidor LDAP LDAP: %s. Extensiones PHP: el plugin LDAP requiere la extensión ldap. Por favor instalar e activarla. Base del DN Usuario 